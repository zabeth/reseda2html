#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 15:42:00 2019
Lecture du fichier des permanents
@author: zabeth

On veut obtenir ça : 
<ul class="people">
    <li class="nom">
       <a href="https://perso.limsi.fr/zabeth/">Elisabeth PIOTELAT </b></a>
    </li>
    <li class="tel">01 69 15 8258</li>
    <li class="groupe">AMIC - Administration des Moyens Informatiques Communs</li>
</ul>

"""
import csv
import json


# Affiche le nom (a)
def entreeli(prenom,nom,url,tel,titre):
   ligneli="\n  <li class=\"nom\">"
   ligneidentite = prenom + " " + nom
   if len(url) > 0:
      ligneidentite="<a href="+url+">"+ligneidentite+"</a>"
   ligneli += ligneidentite +"</li>"
   if len (tel) > 0:
      ligneli +="\n  <li class=\"tel\">"+tel+"</li>"
   if len (titre) > 0:
       ligneli +="\n  <li class=\"titre\">"+titre+"</li>"
   return ligneli
      
         
          

# Lit in fichier Json
def litjson(fichiernom):
    try:  
        f=open (fichiernom)
        f.close
        with open(fichiernom) as json_data:
            data_dict = json.load(json_data)
            
    except FileNotFoundError:
        data_dict = {}

    return data_dict
        

# Trouve le nom du fichier json
# Normalement NOM.json mais peut différer en cas d'homonymes
def trouvejson(nom,prenom,jsondir):
    # On prend le nom du dossier + nom de la personne
    jsonfic = jsondir + "/" + nom + ".json"
    
    # On enlève les espaces
    jsonfic = jsonfic.replace(" ","")
    
    # On teste 
    test = litjson (jsonfic)
    
    # On regarde s'il y a des homonymes
    if  ('homonymes' in test):
       if (prenom in test):
          jsonfic = jsondir+"/"+test[prenom]

    return jsonfic
    
def liensinternes (chaine, tag):
    indexalpha = "<p>\n"
    for lettre in chaine:
        indexalpha += "<a href=\"#"+tag+lettre+"\">"
        indexalpha += lettre
        indexalpha += "</a> . \n"
    indexalpha += "</p>\n"
    return indexalpha
    
# Ecrit
# <ul>  
# <li class="nom">Elisabeth PIOTELAT  </li>  
# <li class="groupe">AMIC - Administration des Moyens Informatiques Communs </li>
# </ul>
def entreeul(fichiercsv,jsondir,tag):
   oldtest = ""
   contenu = ""
# Passe à 1 lorsque l'on a ouvert une balise ul
   ul = 0 
   ancre = ""
   calpha = ""
   with open(fichiercsv, newline='') as csvfile:
       lecteur=csv.reader(csvfile, delimiter=';')
       for individu in lecteur:
          ligneul = ""
          telephone = ""
          www = ""
          titre = ""
          nom = individu [0]
          
          prenom = individu [1]
          groupe = individu [2]
        
          jsonfic = trouvejson (nom,prenom,jsondir)
          
          test=nom+prenom
        #Si on a un nouveau nom, on l'affiche
          if test != oldtest:
              if ul==1:
                  ligneul = "\n</ul>\n\n"
              
              if tag != "non": 
                  if nom[0] != ancre:
                      ancre = nom[0]
                      calpha += ancre
                      ligneul += "<a name=\""+tag+ancre+"\">"
                      ligneul += "<h3 class=\"ancre\">"+ancre+"</h3></a>\n"
              ligneul += "<ul>"
              ul = 1
              infos = litjson (jsonfic)        
              if ('Tel' in infos):
                  telephone = infos['Tel']
              if ('Web' in infos):
                  www = infos['Web']
              if ('Fonction' in infos):
                  titre = infos['Fonction']
              ligneul += entreeli(prenom,nom,www,telephone,titre)
              oldtest=test

          ligneul  +="\n  <li class=\"groupe\">"+groupe+"</li>"
          contenu +=ligneul
          
   if ul==1:
         contenu +="\n</ul>\n"
   
   if calpha != "":
       tabindex = liensinternes(calpha, tag)
       contenu = tabindex + contenu
       
   return contenu      
   

def entreetr(nom,prenom,jsondir,langue):
    texte = "<tr>\n"
    fic=trouvejson(nom,prenom,jsondir) 
    
    infos = litjson(fic)

    # Vignette
    img = ""
    if 'Photo' in infos :
      if infos['Photo'] != "": 
         img = "<img src=\""+infos['Photo'] +"\">\n"
        
    texte += "  <td>\n"+img+"  </td>\n"
    # Nom
    texte += "  <td>\n"
    identite = "    <span class=\"nom\">"+prenom+" "
    identite += nom+" </span>\n"
    if ('Web' in infos):
        texte += "   <a href=\""+infos['Web']+"\">\n"+identite+"    </a>\n"
    else:
        texte += identite
    if ('Fonction' in infos) and langue == "fr":
        texte += "<br/>"+infos['Fonction']+"\n"
    
    if ('Title' in infos) and langue == "en":
        texte += "<br/>"+infos['Title']+"\n"
          
    
    texte += "  </td>\n"
    texte += "</tr>\n"
    return texte


