#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 12 13:59:15 2022
A partir d'un export csv, le but est de créer un premier fichier .json 
pour import dans Wordpress
@author: zabeth
"""

import csv

# Le fichier contenant l'export à partir de reseda 
# On peut remplacer par cdd.csv si on ne veut qu'un seul
# A nettoyer 
# 
fichiercsv = "tous.csv"

#Creation d'un bout du fichier .json
def ecritjson(id,nom,prenom,groupe) :
    print ('{')
    idlisn = int(id)
    print ("   \"id\":"+str(idlisn)+",")
    print ("   \"nom\": \""+nom+"\",")
    print ("   \"prenom\": \""+prenom+"\",")
    print ("   \"Equipe\": [\""+groupe+"\"],")
    tableauvide("Departement")
    tableauvide("Fonction")
    tableauvide("Title")
    champsvide("Adresse")
    champsvide("Tel")
    champsvide("Mobile")
    champsvide("Web")
    champsvide("email")
    champsvide("Photo")
    print ("   \"IdHAL\": \"\"")
    print ('},')
    
    
    
def champsvide(quoi):
    print ("   \""+quoi+"\": \"\",")

def tableauvide(quoi):
     print ("   \""+quoi+"\": [],")
     
# Début à supprimer si on veut concaténer. 
print("{")
print("\"membres\":[")
     
with open(fichiercsv, newline='') as csvfile:
       lecteur=csv.reader(csvfile, delimiter=';')
       id = 0;
       for individu in lecteur:
               nom = individu [0]
               prenom = individu [1]
               groupe = individu [2]
               g = groupe.split (" - ")
               id = id +1
               ecritjson(id,nom,prenom,g[0])


#Fin 
print ("{}")
print ("   ]")
print ("}")
