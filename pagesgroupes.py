#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 15:42:00 2019
Affichage des pages des groupes
@author: zabeth

On veut obtenir ça : 
<ul class="people">
    <li class="nom">
       <a href="https://perso.limsi.fr/zabeth/">Elisabeth PIOTELAT </b></a>
    </li>
    <li class="tel">01 69 15 8258</li>
    <li class="groupe">AMIC - Administration des Moyens Informatiques Communs</li>
</ul>

"""
import csv
import configparser
from datetime import datetime
import affichageweb

config = configparser.RawConfigParser()
config.read ('fichiers.cfg')
          
# Où sont les fichiers à traiter
racine=config.get('CONF','racine')
perm = config.get('EXPORT','permanents')
doc = config.get('EXPORT','doctorants')
cdd = config.get('EXPORT','cdd')

# Où sont les informations spécifiques aux utilisateurs
infouser = config.get('CONF','info')

#Où doit-on écrire ? 
sortie = config.get('CONF','sortie')
permh = config.get('OUT','permanents')

maintenant = datetime.now()
# %d : jour du mois
# %m : mois
# %Y : année sur 4 chiffres
datefr= datetime.strftime(maintenant,"%d/%m/%Y")
dateen= datetime.strftime(maintenant,"%Y-%m-%d")




liste= ("VENISE","ILES","CPU","TLP","AMI","AMIC","P2I","SAFT","CTEMO","COMET","DATAFLOT")

# Extrait les membres du groupe d'un fichie CSV. 
def extrait(fichier, verifgroupe, responsable, json, langue, coresponsable):
    texte = ""
    with open(fichier, newline='') as csvfile:
       lecteur=csv.reader(csvfile, delimiter=';')
       for individu in lecteur:
          nom = individu [0]
          prenom = individu [1]
          testgroupe = individu [2]
          if verifgroupe in testgroupe:
              if nom != responsable and nom != coresponsable:
                 texte += affichageweb.entreetr(nom,prenom,json,langue)     
    return texte

for grp in liste:

    
    # En Français
    htmlf = sortie + "/membres."+grp +".html"  
    fr = open(htmlf,"w",encoding="utf8")

    # En Anglais
    htmle = sortie + "/"+grp+".members.html"  
    en = open(htmle,"w",encoding="utf8")

    # On récupère le nom du responsable
    nomresp = config.get(grp,'responsable')
    prenomresp =  config.get(grp,'prenom')
    titregrp = config.get(grp,'titre')
    bonus = ""

    # On récupère le nombre de responsables
    nbhead = config.get(grp,'nbhead')
    
    if nbhead == "2" :
      # On récupère le nom du coresponsable
      nomcoresp = config.get(grp,'coresponsable')
      bonus = nomcoresp
      prenomcoresp =  config.get(grp,'coprenom')
      fr.write ("<h3>Responsables</h3>\n\n")

    if nbhead == "1" :
      fr.write ("<h3>Responsable</h3>\n\n")

    fr.write ("<table class=\"listMember\">\n")

    # En anglais
    en.write ("<h3>Head</h3>\n\n")
    en.write ("<table class=\"listMember\">\n")

    # Affichage du responsable
    donnees = affichageweb.entreetr(nomresp,prenomresp,infouser,"fr")
    data = affichageweb.entreetr(nomresp,prenomresp,infouser,"en")
    fr.write (donnees)  
    en.write (data)  

    # Affichage du coresponsable
    if nbhead == "2" :
      donnees = affichageweb.entreetr(nomcoresp,prenomcoresp,infouser,"fr")
      data = affichageweb.entreetr(nomcoresp,prenomcoresp,infouser,"en")
      fr.write (donnees)  
      en.write (data)  

    fr.write ("</table>\n")    
    
    en.write ("</table>\n")   

    # Affichage des permanents
    
    # Permanents 
    fich = racine + "/" + perm
    test = grp + " - "
    donnees = extrait (fich,test,nomresp,infouser,"fr",bonus)
    data  = extrait (fich,test,nomresp,infouser,"en",bonus)
    
    fr.write ("<h3>Permanents</h3>\n\n")
    fr.write ("<table class=\"listMember\">\n")
    fr.write (donnees)  
    fr.write ("</table>\n")
    
    en.write ("<h3>Academic and Research Staff</h3>\n\n")
    en.write ("<table class=\"listMember\">\n")
    en.write (data)  
    en.write ("</table>\n")   
    
    # Doctorants
    fich = racine + "/" + doc
    donnees = extrait (fich,test,nomresp,infouser,"fr",bonus)
    data  = extrait (fich,test,nomresp,infouser,"en",bonus)
    
    if donnees != "":
      fr.write ("<h3>Doctorants</h3>\n\n")
      fr.write ("<table class=\"listMember\">\n")
      fr.write (donnees)  
      fr.write ("</table>\n")
    
    if data != "":
      en.write ("<h3>PhD Students</h3>\n\n")
      en.write ("<table class=\"listMember\">\n")
      en.write (data)  
      en.write ("</table>\n")   
    
    
    # CDD
    fich = racine + "/" + cdd
    donnees = extrait (fich,test,nomresp,infouser,"fr",bonus)
    data  = extrait (fich,test,nomresp,infouser,"en",bonus)
    
    if donnees != "":
      fr.write ("<h3>CDD</h3>\n\n")
      fr.write ("<table class=\"listMember\">\n")
      fr.write (donnees)  
      fr.write ("</table>\n")
    
    if data != "":
      en.write ("<h3>Other</h3>\n\n")
      en.write ("<table class=\"listMember\">\n")
      en.write (data)  
      en.write ("</table>\n")   



    # Fin
    fr.write ("<p>Données extraites de l'annuaire du CNRS le "+datefr+"</p>\n")
    fr.close
    
    en.write ("<p>Data from CNRS directory at "+dateen+"</p>\n")
    en.close



    

# Recuperation une seule fois des permanents 
#source="/home/zabeth/Dev/datacomm/reseda/permanents.csv"
#recupperm (source,infouser)
