#!/bin/bash

if [ $1 ]; then
  src=$1
else 
  echo "Usage : ./nettoyage.sh nomdufichier"
  exit 0
fi

if [ -e $src ]; then 
  statut="OK"
else
  echo "Fichier $src non présent"
  exit 0
fi

#echo "Nom du fichier à traiter : "
#read src

# Resultat 
out="/tmp/OKreseda.csv"
tmp="/tmp/modifreseda.csv"

#On sépare les lignes avec affectations multiples
#SCOUBIDOU;Sami;"TLP - Traitement du Langage Parlé - 50%
#ILES - Information, Langues Ecrite & Signée - 50%";Présent

# Ceux qui sont à 100% vont ds tmp
grep "100" $src > $out
grep "\"" $src > $tmp
awk -f multi.awk $tmp >> $out
sed -f modif.sed $out > $tmp
sort $tmp > $out
cat $out
