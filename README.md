# reseda 2 html

Le site du LIMSI est du JOOMLA. Les pages d'annuaire sont créées à partir d'un export de RESEDA. 

## Récupération des données à partir de RESEDA

Il faut avoir un compte ayant accès à la liste du personnel. 

### Création d'une vue spécifique pour le web.

Pour chaque catégorie de personnel (permanent, doctorant, cdd), une vue permet de ne sélectionner que les informations suivantes :

* Nom
* Prénom
* Groupe
* Présent

![Export](reseda2.png)

![Export](reseda3.png)

![Export](reseda4.png)


### Export

![Options Export](reseda5.png)

Après export , on obtient 3 fichiers csv, par exemple :

* permaments.csv
* doctorants.csv
* cdd.csv


Pour chaque personne présente au LIMSI, le fichier .csv correspondant contient une ligne : 

PIOTELAT;Elisabeth;AMIC - Administration des Moyens Informatiques Communs - 100%;Présent


## Traitement des fichiers exportés

### Nettoyage

Le script nettoyage.sh permet de supprimer les informations inutiles pour n'obtenir que : 

PIOTELAT;Elisabeth;AMIC - Administration des Moyens Informatiques Communs 

Il effectue quelques modifications pour les personnes appartenant à plusieurs groupes ou souhaitant que le nom affiché dans l'annuaire ne soit pas celui de Reseda.

On le lance : 
./nettoyage.sh permanents.csv > permOK.csv
./nettoyage.sh doctorants.csv > docOK.csv
./nettoyage.sh cdd.csv > docOK.csv

### Modification des données

Les informations qui ne figurent pas dans l'annuaire (page perso, etc...) sont dans des fichiers .json (voir PIOTELAT.json) que les utilisateurs peuvent modifier via un formulaire.

### Mise à jour des pages web

 On peut générer soit tout l'annuaire, soit uniquement une partie de celui-ci (par exemple, uniquement les CDD, s'il n'y a pas de changement chez les permanents et doctorants).

Pour cela, sur le serveur de test, on exécute les commandes correspondantes : 

python3 lectureperm.py
python3 lecturecdd.py
python3 lecturedoc.py
python3 pagesgroupes.py

